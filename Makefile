test:
	pytest tests.py -xs
coverage:
	pytest --cov=main --cov-report term-missing tests.py

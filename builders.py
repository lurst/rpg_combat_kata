from domain import Fighter, Ranged


class CharacterBuilder:

    def __init__(self):
        raise NotImplemented()

    def set_dead(self):
        self.character.health = 0
        self.character.alive = False
        return self

    def set_health(self, health):
        self.character.health = health
        return self

    def set_level(self, level):
        self.character.level = level
        return self

    def join_faction(self, faction):
        self.character.factions.append(faction)
        return self

    def build(self):
        return self.character


class FighterBuilder(CharacterBuilder):

    def __init__(self):
        self.character = Fighter()


class RangedBuilder(CharacterBuilder):

    def __init__(self):
        self.character = Ranged()

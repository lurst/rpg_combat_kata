from exceptions import (
    CannotDamageAllyError,
    CannotDamageSelfError,
    CannotHealOthersError,
    CannotTargetDestroyedPropError,
)


class HealthyBeing:

    def __init__(self, health):
        self.health = health

    def receive_damage(self, character, distance):
        if self.destroyed:
            raise CannotTargetDestroyedPropError()

        self.health -= character.damage
        if self.health <= 0:
            self.health = 0


class Prop(HealthyBeing):

    @property
    def destroyed(self):
        if self.health == 0:
            return True
        return False


class Character(HealthyBeing):

    def __init__(self):
        self.alive = True
        self.level = 1
        self.damage = 10
        self.healing = 10
        self.factions = []

        # Set health
        super().__init__(1000)

    def receive_damage(self, character, distance):
        if character == self:
            raise CannotDamageSelfError()

        if self.is_ally(character):
            raise CannotDamageAllyError()

        if character.max_range <= distance:
            return

        damage = character.damage
        if self.level + 5 <= character.level:
            damage += damage / 2
        elif self.level >= character.level + 5:
            damage -= damage / 2

        self.health -= int(damage)
        if self.health <= 0:
            self.health = 0
            self.alive = False

    def is_ally(self, character):
        for faction in self.factions:
            if faction in character.factions:
                return True
        return False

    def receive_healing(self, character):
        if character != self and not self.is_ally(character):
            raise CannotHealOthersError()
        if self.health == 0:
            return

        self.health += character.healing
        if self.health >= 1000:
            self.health = 1000

    @property
    def is_dead(self):
        return self.health == 0 and self.alive is False


class Fighter(Character):

    def __init__(self):
        super().__init__()
        self.max_range = 2


class Ranged(Character):

    def __init__(self):
        super().__init__()
        self.max_range = 20


class Faction:

    def __init__(self):
        pass

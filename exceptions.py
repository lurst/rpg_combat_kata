class CannotDamageSelfError(Exception):
    pass


class CannotDamageAllyError(Exception):
    pass


class CannotHealOthersError(Exception):
    pass


class CannotTargetDestroyedPropError(Exception):
    pass

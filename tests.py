import pytest

from domain import Faction, Prop
from exceptions import (
    CannotDamageAllyError,
    CannotDamageSelfError,
    CannotHealOthersError,
    CannotTargetDestroyedPropError,
)
from builders import (
    FighterBuilder,
    RangedBuilder,
)


class TestCharacterCreation:

    @staticmethod
    def test_fighter_character_creation():
        char = FighterBuilder().build()

        assert char.alive is True
        assert char.health == 1000
        assert char.level == 1
        assert char.damage == 10
        assert char.healing == 10
        assert char.max_range == 2
        assert char.factions == []

    @staticmethod
    def test_ranged_character_creation():
        char = RangedBuilder().build()

        assert char.alive is True
        assert char.health == 1000
        assert char.level == 1
        assert char.damage == 10
        assert char.healing == 10
        assert char.max_range == 20
        assert char.factions == []


class TestFactions:

    @staticmethod
    def test_allies():
        faction = Faction()
        char = FighterBuilder().join_faction(faction).build()
        other_char = FighterBuilder().join_faction(faction).build()

        assert char.is_ally(other_char) is True

    @staticmethod
    def test_non_allies():
        faction = Faction()
        another_faction = Faction()
        char = FighterBuilder().join_faction(faction).build()
        other_char = FighterBuilder().join_faction(another_faction).build()

        assert char.is_ally(other_char) is False


class TestHealing:

    @staticmethod
    def test_character_healing_damaged_self():
        char = FighterBuilder().set_health(900).build()

        char.receive_healing(char)

        assert char.health == 910

    @staticmethod
    def test_character_healing_damaged_ally():
        faction = Faction()
        char = FighterBuilder().join_faction(faction).build()
        other_char = FighterBuilder().join_faction(faction).set_health(900).build()

        other_char.receive_healing(char)

        assert other_char.health == 910

    @staticmethod
    def test_character_healing_full_hp_self():
        char = FighterBuilder().build()

        char.receive_healing(char)

        assert char.health == 1000

    @staticmethod
    def test_character_healing_dead_self():
        char = FighterBuilder().set_dead().build()

        char.receive_healing(char)

        assert char.is_dead

    @staticmethod
    def test_character_healing_other_non_ally_character():
        char = FighterBuilder().build()
        other_char = FighterBuilder().build()

        with pytest.raises(CannotHealOthersError):
            other_char.receive_healing(char)


class TestDealingDamage:

    @staticmethod
    def test_character_dealing_damage_to_enemy():
        char = FighterBuilder().build()
        enemy = FighterBuilder().build()

        enemy.receive_damage(char, 1)

        assert enemy.health == 990

    @staticmethod
    def test_character_dealing_damage_to_prop():
        char = FighterBuilder().build()
        prop = Prop(1000)

        prop.receive_damage(char, 1)

        assert prop.health == 990

    @staticmethod
    def test_character_dealing_damage_to_destroyed_prop():
        char = FighterBuilder().build()
        prop = Prop(0)

        with pytest.raises(CannotTargetDestroyedPropError):
            prop.receive_damage(char, 1)

    @staticmethod
    def test_character_dealing_damage_to_ally():
        faction = Faction()
        char = FighterBuilder().join_faction(faction).build()
        ally = FighterBuilder().join_faction(faction).build()

        with pytest.raises(CannotDamageAllyError):
            ally.receive_damage(char, 1)

    @staticmethod
    def test_fighter_dealing_damage_from_afar():
        char = FighterBuilder().build()
        enemy = FighterBuilder().build()

        enemy.receive_damage(char, 10)

        assert enemy.health == 1000

    @staticmethod
    def test_ranged_dealing_damage_from_afar():
        char = RangedBuilder().build()
        enemy = FighterBuilder().build()

        print(char.max_range, 21)
        enemy.receive_damage(char, 21)

        assert enemy.health == 1000

    @staticmethod
    def test_ranged_dealing_damage_within_range():
        char = RangedBuilder().build()
        enemy = FighterBuilder().build()

        enemy.receive_damage(char, 19)

        assert enemy.health == 990

    @staticmethod
    def test_character_dealing_damage_to_low_hp():
        char = FighterBuilder().build()
        enemy = FighterBuilder().set_health(5).build()

        enemy.receive_damage(char, 1)

        assert enemy.is_dead

    @staticmethod
    def test_higher_level_damage():
        char = FighterBuilder().set_level(6).build()
        enemy_char = FighterBuilder().build()

        enemy_char.receive_damage(char, 1)

        assert enemy_char.health == 985

    @staticmethod
    def test_lower_level_damage():
        char = FighterBuilder().build()
        enemy_char = FighterBuilder().set_level(6).build()

        enemy_char.receive_damage(char, 1)

        assert enemy_char.health == 995

    @staticmethod
    def test_character_dealing_damage_to_itself():
        char = FighterBuilder().build()

        with pytest.raises(CannotDamageSelfError):
            char.receive_damage(char, 1)
